#pragma once
/////////////////////////////////////////////////////////////////////
// Server.h - Provides server funtionality, runs code publisher		//
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu          //
// CSE687 - OOD, Spring 2017									   //
/////////////////////////////////////////////////////////////////////
/*
Operation :
Receives request from clients though sockets and process each clients request
Has to classes, 
Server - Executes the request received by client handler and sends back appropriate messages to the client
ClientHendler - used by socket listen to receive messages and files from clients

Public Interface :
Server s(8080);		//Takes server port number as argument
s.makeMessage(postType, strMsg, server_endpoint)		//Create httpmessage,
											postType should be selected based on the use
											and we make sure clinet know what to do for a given postType
s.sendFile(fqname, category, srv_endpoint, socket)		// sends file to store in folder spcified to client
s.sendMessage(httpMessage, socket)		// sends httpsmessage thought an already connected socket


ClientHandler ch(blockingQ) // class with operator() method, directly used by socketlistner
						member funtions readMessage and readFile are called by operator() not used by user directly

Required FIles :
Server.h, Server.cpp
HttpeMessage.h, HttpMessage.cpp
Sockets.h, Sockets.cpp
Cpp11-BlockingQueue.h, Cpp11-BlockingQueue.cpp
FileSystem,h, Filesystem.cpp
CodePublisher.h, CodePublisher.cpp


Build Command:
devenv Server.sln /rebuild debug

Maintanance History:
Ver 1.0 - May 4,2017 - Initial Release
*/
#include"../HttpMessage/HttpMessage.h"
#include"../Sockets/Sockets.h"
#include"../Cpp11-BlockingQueue/Cpp11-BlockingQueue.h"
class ClientHandler
{
public:
	ClientHandler(Async::BlockingQueue<HttpMessage>& msgQ) : msgQ_(msgQ) {}
	void operator()(Socket socket);
private:
	bool connectionClosed_;
	HttpMessage readMessage(Socket& socket);
	bool readFile(HttpMessage& msg, Socket& socket);
	Async::BlockingQueue<HttpMessage>& msgQ_;
};

class Server
{
public:
	Server(std::string port) { myEndPoint = "localhost:" + port; }
	using EndPoint = std::string;
	void handleFileReq(HttpMessage msg);
	void handlePublishCmd(HttpMessage msg);
	void handleDirsReq(HttpMessage msg);
	void handleFilesListReq(HttpMessage msg);
	void handleDltReq(HttpMessage msg);
	HttpMessage makeMessage(std::string postType, const std::string& msgBody, const EndPoint& ep);
	void sendMessage(HttpMessage& msg, Socket& socket);
	bool sendFile(const std::string& fqname, const std::string& category, const EndPoint& ep, Socket& socket);
private:
	EndPoint myEndPoint = "";

};
