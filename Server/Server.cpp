/////////////////////////////////////////////////////////////////////
// Server.h - Provides server funtionality, runs code publisher		//
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu          //
// CSE687 - OOD, Spring 2017									   //
/////////////////////////////////////////////////////////////////////

#include"Server.h"
#include"../FileSystem/FileSystem.h"
#include"../CodePublisher/CodePublisher.h"
#include<string>
#include<iostream>
using namespace std;

// Used as funtor/Callable object by socketlistner provided in sockets package
void ClientHandler::operator()(Socket socket)
{
	while (true)
	{
		HttpMessage msg = readMessage(socket);
		if (connectionClosed_ || msg.bodyString() == "quit")
		{
			//std::cout << "Server's Receiver Stopped" << std::endl;
			break;
		}
		msgQ_.enQ(msg);
	}
}

// Reads message from socket and creates httpmessage
HttpMessage ClientHandler::readMessage(Socket& socket)
{
	connectionClosed_ = false;
	HttpMessage msg;
	while (true)
	{
		std::string attribString = socket.recvString('\n');
		if (attribString.size() > 1)
		{
			HttpMessage::Attribute attrib = HttpMessage::parseAttribute(attribString);
			msg.addAttribute(attrib);
		}
		else
		{
			break;
		}
	}
	// If client is done, connection breaks and recvString returns empty string
	if (msg.attributes().size() == 0)
	{
		connectionClosed_ = true;
		return msg;
	}
	// read body if POST
	std::string postType = msg.findValue("POST");
	if (postType == "File")
	{
		bool success = readFile(msg, socket);
		if (success)
			msg.addAttribute(HttpMessage::Attribute("copyStatus", "Sucess"));
		else
			msg.addAttribute(HttpMessage::Attribute("copyStatus", "Fail"));
	}
	else
	{
		// read message body
		size_t numBytes = 0;
		size_t pos = msg.findAttribute("content-length");
		if (pos < msg.attributes().size())
		{
			numBytes = Utilities::Converter<size_t>::toValue(msg.attributes()[pos].second);
			Socket::byte* buffer = new Socket::byte[numBytes + 1];
			socket.recv(numBytes, buffer);
			buffer[numBytes] = '\0';
			std::string msgBody(buffer);
			msg.addBody(msgBody);
			delete[] buffer;
		}
	}
	return msg;
}

// Reads frile from socket, called after reciving a httpmessage specifying a file is being sent as stream
bool ClientHandler::readFile(HttpMessage& msg, Socket& socket)
{
	size_t contentSize;
	std::string filename = msg.findValue("filename");
	std::string category = msg.findValue("category");
	std::string sizeString = msg.findValue("content-length");
	if (sizeString != "") contentSize = Utilities::Converter<size_t>::toValue(sizeString);
	else return false;
	FileSystem::Directory::create("../PublisherStorage/" + category);
	std::string fqname = "../PublisherStorage/" + category + "/" + filename;
	FileSystem::File file(fqname);
	file.open(FileSystem::File::out, FileSystem::File::binary);
	const size_t BlockSize = 2048;
	Socket::byte buffer[BlockSize];
	size_t bytesToRead;
	FileSystem::Block blk;
	if (!file.isGood())
	{		
		std::cout << " can't open file " + fqname;
		while (true)
		{
			if (contentSize > BlockSize) bytesToRead = BlockSize;
			else bytesToRead = contentSize;
			socket.recv(bytesToRead, buffer);
			for (size_t i = 0; i < bytesToRead; ++i) blk.push_back(buffer[i]);
			if (contentSize < BlockSize) break;
			contentSize -= BlockSize;
		}
		return false;
	}
	while (true)
	{
		if (contentSize > BlockSize) bytesToRead = BlockSize;
		else bytesToRead = contentSize;
		socket.recv(bytesToRead, buffer);
		FileSystem::Block blk;
		for (size_t i = 0; i < bytesToRead; ++i) blk.push_back(buffer[i]);
		file.putBlock(blk);
		if (contentSize < BlockSize) break;
		contentSize -= BlockSize;
	}
	file.close();
	return true;
}

// creates HttpMessages to send to client
HttpMessage Server::makeMessage(string postType, const std::string& msgBody, const EndPoint& ep)
{
	HttpMessage msg;
	msg.clear();
	msg.addAttribute(HttpMessage::attribute("POST", postType));
	msg.addAttribute(HttpMessage::attribute("to", ep));
	msg.addAttribute(HttpMessage::attribute("from", myEndPoint));
	if (msgBody.size() > 0)
	{
		msg.addBody(msgBody);
		msg.addAttribute(HttpMessage::attribute("content-length", std::to_string(msgBody.size())));
	}
	return msg;
}

// Sends a Httpmessage to client through already connected socket
void Server::sendMessage(HttpMessage& msg, Socket& socket)
{
	std::string msgString = msg.toString();
	socket.send(msgString.size(), (Socket::byte*)msgString.c_str());
}

// Sends file to a client though already connected socket
bool Server::sendFile(const std::string& fqname, const std::string& category, const EndPoint& ep, Socket& socket)
{

	FileSystem::FileInfo fi(fqname);
	size_t fileSize = fi.size();
	std::string sizeString = Utilities::Converter<size_t>::toString(fileSize);
	FileSystem::File file(fqname);
	file.open(FileSystem::File::in, FileSystem::File::binary);
	if (!file.isGood())
		return false;

	HttpMessage msg = makeMessage("File", "", ep);
	msg.addAttribute(HttpMessage::Attribute("filename", FileSystem::Path::getName(fqname)));
	msg.addAttribute(HttpMessage::Attribute("content-length", sizeString));
	msg.addAttribute(HttpMessage::Attribute("category", category));
	sendMessage(msg, socket);
	const size_t BlockSize = 2048;
	Socket::byte buffer[BlockSize];
	while (true)
	{
		FileSystem::Block blk = file.getBlock(BlockSize);
		if (blk.size() == 0)
			break;
		for (size_t i = 0; i < blk.size(); ++i)
			buffer[i] = blk[i];
		socket.send(blk.size(), buffer);
		if (!file.isGood())
			break;
	}
	file.close();
	return true;
}

// helper funtion to handle file request
void Server::handleFileReq(HttpMessage msg)
{
	string clientEp = msg.findValue("from");
	size_t pos = clientEp.find(":");
	string clientip = clientEp.substr(0, pos);
	string clientport = clientEp.substr(pos + 1);
	SocketConnecter si;
	int retries = 5;
	bool connectionSucess = true;
	while (!si.connect(clientip, std::stoi(clientport)))
	{
		std::cout << "\n Server waiting to connect to " + clientip + ":" + clientport;
		::Sleep(100);
		retries--;
		if (retries == 0)
		{
			std::cout << "\n Couldn't connect to client";
			connectionSucess = false;
			break;
		}
	}
	if (connectionSucess) {
		vector<string> requestedFiles= Utilities::StringHelper::split(msg.bodyString());
		for (string file : requestedFiles)
		{
			string fqname = FileSystem::Path::getFullFileSpec(file);
			string fext = FileSystem::Path::getExt(file);
			string category = "NULL";
			if (fext == "js")
				category = "scripts";
			else if (fext == "css")
				category = "styles";
				if (sendFile(fqname, category, clientEp, si))
					cout << "File sent : " + file << endl;
				else
					cout << "Failed to send file : " + file << endl;
		}
	}
}

// helper funtion to handle publish request
void Server::handlePublishCmd(HttpMessage msg)
{
	vector<string> argsVector = Utilities::StringHelper::split(msg.bodyString());
	size_t argsCount = argsVector.size();
	char** args = new char*[argsCount];
	for (size_t i = 0; i < argsCount; i++)
	{
		args[i] = const_cast<char*>(argsVector[i].c_str());
	}
	cout << "Publishing files at : " << args[1] << endl;
	CodePublisher cp;
	cp.processCommandLineArgs(argsCount, args);
	cp.createIndex();
	cp.publish();
	string pubPath = FileSystem::Path::getFullFileSpec("../PublisherStorage/PublishedFiles/");
	vector<string> pubedFiles = FileSystem::Directory::getFiles(pubPath, "*.htm");
	string clientEp = msg.findValue("from");
	size_t pos = clientEp.find(":");
	string clientip = clientEp.substr(0, pos);
	string clientport = clientEp.substr(pos + 1);
	SocketConnecter si;
	si.connect(clientip, std::stoi(clientport));
	for (string file : pubedFiles)
	{
		cout << "Sending file : " + pubPath + file << endl;
		sendFile(pubPath + file, "NULL", clientEp, si);
	}
}

// helper funtion to get directories recursively
void getdirshelper(vector<string>& finalDirs,string path)
{
	vector<string> foundDirs = FileSystem::Directory::getDirectories(path);
	for (string dir : foundDirs)
	{
		vector<string> subdirs;
		if (dir != "." && dir != "..") {
			finalDirs.push_back(path + "/" + dir);
			getdirshelper(finalDirs, path + "/" + dir);
		}
	}
}

// helper funtion to handle to directories request
void Server::handleDirsReq(HttpMessage msg)
{
	string clientEp = msg.findValue("from");
	size_t pos = clientEp.find(":");
	string clientip = clientEp.substr(0, pos);
	string clientport = clientEp.substr(pos + 1);
	SocketConnecter si;
	int retries = 5;
	bool connectionSucess = true;
	while (!si.connect(clientip, std::stoi(clientport)))
	{
		std::cout << "\n Server waiting to connect to " + clientip + ":" + clientport;
		::Sleep(100);
		retries--;
		if (retries == 0)
		{
			std::cout << "\n Couldn't connect to client";
			connectionSucess = false;
			break;
		}
	}
	if (connectionSucess) {
		vector<string> dirs;
		string root = "../PublisherStorage";
		getdirshelper(dirs, root);
		string dirsString = "";
		for (string dir : dirs)
			dirsString += dir + ",";
		HttpMessage msgToClient = this->makeMessage("Dirs", dirsString, msg.findValue("from"));
		this->sendMessage(msgToClient, si);
		cout << "Sent Dirs list to client" << endl;
	}
}

// helper funtion to handle file request
void Server::handleFilesListReq(HttpMessage msg)
{
	string clientEp = msg.findValue("from");
	size_t pos = clientEp.find(":");
	string clientip = clientEp.substr(0, pos);
	string clientport = clientEp.substr(pos + 1);
	SocketConnecter si;
	int retries = 5;
	bool connectionSucess = true;
	while (!si.connect(clientip, std::stoi(clientport)))
	{
		std::cout << "\n Server waiting to connect to " + clientip + ":" + clientport;
		::Sleep(100);
		retries--;
		if (retries == 0)
		{
			std::cout << "\n Couldn't connect to client";
			connectionSucess = false;
			break;
		}
	}
	if (connectionSucess) {
		string dir = msg.bodyString();
		vector<string> files = FileSystem::Directory::getFiles(dir);
		string filesString = "";
		for (string file : files)
			filesString += dir +"/"+ file + ",";
		HttpMessage msgToClient = this->makeMessage("FilesList", filesString, clientEp);
		this->sendMessage(msgToClient, si);
		cout << "Sent files list to client" << endl;
	}
}

// helper funtion to handle delete request
void Server::handleDltReq(HttpMessage msg)
{
	string clientEp = msg.findValue("from");
	size_t pos = clientEp.find(":");
	string clientip = clientEp.substr(0, pos);
	string clientport = clientEp.substr(pos + 1);
	SocketConnecter si;
	int retries = 5;
	bool connectionSucess = true;
	while (!si.connect(clientip, std::stoi(clientport)))
	{
		std::cout << "\n Server waiting to connect to " + clientip + ":" + clientport;
		::Sleep(100);
		retries--;
		if (retries == 0)
		{
			std::cout << "\n Couldn't connect to client";
			connectionSucess = false;
			break;
		}
	}
	if (connectionSucess) {
		vector<string> files = Utilities::StringHelper::split(msg.bodyString());
		for (string file : files)
		{
			string fqname = FileSystem::Path::getFullFileSpec(file);			
			FileSystem::File::remove(fqname);
		}
	}
}

// funtion to handle received requests
void serverMsgHandler(Async::BlockingQueue<HttpMessage>& msgQ,string port)
{
	Server s(port);
	while (true)
	{
		HttpMessage msg = msgQ.deQ();
		string postType = msg.findValue("POST");
		//cout << "\n\n  server recvd message contents:\n" + msg.bodyString() << endl;
		if (msg.bodyString() == "QUIT")
			break;
		else if (postType == "MSG")
			cout << "\n Client recvd message contents:\n" + msg.bodyString() << endl;
		if (postType == "PublishCmd")
		{
			s.handlePublishCmd(msg);
		}
		else if (postType == "File")
		{
			string copyStatus = msg.findValue("copyStatus");
			if (copyStatus == "Fail")
				cout << "Failed to copy " + msg.findValue("filename") << endl;
			else
				cout << "Copied file " + msg.findValue("filename") << endl;
		}
		else if (postType == "FileReq")
		{
			s.handleFileReq(msg);
		}
		else if (postType == "DirsReq")
		{
			cout << "Sending Dirs list" << endl;
			s.handleDirsReq(msg);
		}
		else if (postType == "FilesListReq")
		{
			cout << "Sending files list" << endl;
			s.handleFilesListReq(msg);
		}
		else if (postType == "DltFilesReq")
		{
			cout << "Deleting files" << endl;
			s.handleDltReq(msg);
		}
	}
}

int main(int argc, char* argv[])
{
	::SetConsoleTitle(L"Publisher");
	FileSystem::Directory::setCurrentDirectory("../");
	Async::BlockingQueue<HttpMessage> msgQ;
	try
	{
		size_t port = Utilities::Converter<size_t>::toValue(argv[1]);
		SocketSystem ss;
		SocketListener sl(port, Socket::IP6);
		ClientHandler ch(msgQ);
		SocketConnecter si;
		sl.start(ch);
		cout << "Server listening at port "<< port << endl;
		thread msgHandlerThread(serverMsgHandler, std::ref(msgQ), argv[1]);
		msgHandlerThread.join();

		/* USED FOR TESTING
		while (!si.connect("localhost", 8082))
		{
			std::cout << "\n Server waiting to connect";
			::Sleep(100);
		}
		HttpMessage msg = this->makeMessage("MSG", "Test msg from server", "localhost:8081");
		this->sendMessage(msg, si);
		string filespath = FileSystem::Path::getFullFileSpec("../PublisherStorage/");
		vector<string> foundFiles = FileSystem::Directory::getFiles(filespath, "Child.h.htm");
		for (string foundfile : foundFiles)
		{
			cout << "File found : " + filespath + foundfile << endl;
			if (this->sendFile(filespath + foundfile, "NULL", "localhost:8081", si))
				cout << "File sent : " + foundfile << endl;
		}*/
	}
	catch (std::exception& exc)
	{
		cout << "\n  Exeception caught: ";
		std::string exMsg = "\n  " + std::string(exc.what()) + "\n\n";
		cout << exMsg;
	}
}
