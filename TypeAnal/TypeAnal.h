#pragma once
/////////////////////////////////////////////////////////////////////
// TypeAnal.h -  Create TypeTable from AST						   //
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu          //
// CSE687 - OOD, Spring 2017									     //
/////////////////////////////////////////////////////////////////////
/*
Operation:
	Using AST from code analyzer create type table and add all types to it.

Public Interface:
	TypeAnal ta();		//create object and get AST from code analyzer
	ta.doTypeAnal();	//create typetable and add all types using recursive DFS function
	ta.getTypeTable();	//get TypeTable after it is populated

Required Files:
	TypeAnal.h, TypeAnal.cpp
	TypeTable.h, TypeTable.cpp
	ActionsAndRules.h, ActionsAndRules.cpp

Build Command:
	devenv TypeAnal.sln /rebuild debug

Maintanance History:
	Ver 1.0 - March 7,2017 - Initial Release
*/
#include <iostream>
#include <functional>
#include "../Parser/ActionsAndRules.h"
#include "../TypeTable/TypeTable.h"

namespace CodeAnalysis
{
	// class to analys types and create type table
	class TypeAnal
	{
	public:
		using SPtr = std::shared_ptr<ASTNode*>;

		TypeAnal();
		void doTypeAnal();
		TypeTable getTypeTable();
	private:
		void DFS(ASTNode* root,ASTNode* pNode);
		AbstrSynTree& ASTref_;
		//ScopeStack<ASTNode*> scopeStack_;
		//Scanner::Toker& toker_;
		TypeTable tt;
	};

	inline TypeAnal::TypeAnal() :
		ASTref_(Repository::getInstance()->AST()){}
}
