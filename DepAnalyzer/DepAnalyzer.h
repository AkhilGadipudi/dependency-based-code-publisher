#pragma once
/////////////////////////////////////////////////////////////////////
// DepAnalyzer.h -  Analyze Dependencied or fileset				   //
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu          //
// CSE687 - OOD, Spring 2017									   //
/////////////////////////////////////////////////////////////////////
/*
Operation:
Using Tokenizer, this package tokenizes each file in the given file set
and searches each token in the type table, if the type table has an entry
with the token as key, it means the files being tokenized depends on the file
where the type was created, this info is obtained from typetable

Public Interface:
DepAnalyzer da(rootPath, typetable) // require path to folder containing file set and typetable
da.doDepAnal()						// tokenize each file, check typetable and create DepTable
da.show()							// Prints DepTable
da.showBD()							 // Prints contents of BD to which dependencies were saved
da.toXml(filename)					// Creates xml file, saves DepTable contents
da.getDepTable()					// returns the DepTable created

Required Files:
DepAnalyzer.h, DepAnalyzer.cpp
TypeAnal.h, TypeAnal.cpp
Tokenizer.h, Tokenizer.cpp
FileSystem.h, FileSystem.cpp
XmlDocument.h, XmlDocument.cpp
XmlElement.h, XmlElement.cpp

Build Command:
devenv DepAnalyzer.sln /rebuild debug

Maintanance History:
Ver 1.0 - March 7,2017 - Initial Release
*/
#include"../NoSqlDb/NoSqlDb/NoSqlDb.h"
#include"../TypeAnal/TypeAnal.h"
#include"../Tokenizer/Tokenizer.h"
#include"../FileSystem/FileSystem.h"
using namespace FileSystem;
class DepAnalyzer
{
public:
	DepAnalyzer(std::string root, TypeTable tt, std::vector<std::string> patterns) : 
		root(Path::getFullFileSpec(root)), tt(tt), patterns(patterns){};
	void doDepAnal();
	void show();
	void showDB();
	void toXml(std::string filename);
	std::unordered_map<std::string, std::vector<std::string>> getDepTable();
	std::vector<std::string> getFiles();
	
private:
	TypeTable tt;
	std::string root;
	std::vector<std::string> patterns;
	std::vector<std::string> files;
	NoSqlDb<std::string> db;
	std::vector<std::string> getFilesForAnal(std::string path, std::vector<std::string> patterns);
	std::unordered_map<std::string, std::vector<std::string>> depTable;
	std::vector<std::string> depAnalFile(std::string filePath);
	void addToDB(std::string file, std::vector<std::string> deps);
	std::vector<std::string> rmvSelfDep(std::string file, std::vector<std::string> deps);
};