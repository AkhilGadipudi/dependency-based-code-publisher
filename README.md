# Dependency Based Code Publisher - [Demo Video]( https://youtu.be/mhzQRYv6fVk)
* Code Publisher creates HTML pages of all source code file with links to dependencies which are obtained using Type-Based Dependency Analyzer
* The Type-Based Dependency Analyzer identifies various type definitions, global functions and data using tokenizer in every file to create type table which is then used to figure out the dependencies.
* Used C# and WPF for UI and used CLI as shim to communicate between C# in the UI and C++ in the backend.
* Provided facility to collapse and expand classes, functions, and other blocks of code in the HTML pages using JavaScript.

---
This was developed as part of CSE687 - Object Oriented Design, so some part of code is provided as a starter code by our professor [Dr. Jim Fawcett](http://ecs.syr.edu/faculty/fawcett/handouts/webpages/fawcettHome.htm)
