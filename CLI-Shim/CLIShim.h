#pragma once
//////////////////////////////////////////////////////////////////////
// CLIShim.h- C++\CLI layer is a proxy for calls into MockChannel   //
//																	//
// Akhil Gadipudi - Object Oriented Design, Spring 2017				 //
// Source :: Dr. Jim Fawcett										//
//////////////////////////////////////////////////////////////////////

#include "../Client/IClient.h"
#include <string>
using namespace System;

public ref class Shim
{
public:
  using Message = String;
  Shim();
  ~Shim();
  void postMessage(Message^ msg);
  String^ getMessage();
  void startClient(String^ port);
private:
	String^ stdStrToSysStr(const std::string& str);
	std::string sysStrToStdStr(String^ str);
	IClient* client;
};


