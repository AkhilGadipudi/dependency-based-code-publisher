/////////////////////////////////////////////////////////////////////////
// CLIShim.cpp - C++\CLI layer is a proxy for calls into MockChannel   //
//																		//
// Akhil Gadipudi - Object Oriented Design, Spring 2017					 //
// Source :: Dr. Jim Fawcett											//
/////////////////////////////////////////////////////////////////////////

#include "CLIShim.h"
#include <iostream>

//----< convert std::string to System.String >---------------------------

String^ Shim::stdStrToSysStr(const std::string& str)
{
  return gcnew String(str.c_str());
}
//----< convert System.String to std::string >---------------------------

std::string Shim::sysStrToStdStr(String^ str)
{
  std::string temp;
  for (int i = 0; i < str->Length; ++i)
    temp += char(str[i]);
  return temp;
}
//----< Constructor sets up sender and receiver >------------------------

Shim::Shim()
{
  ObjectFactory factory;
  client = factory.createClient();
}

Shim::~Shim() {
	delete client;
}
//----< put message into channel >---------------------------------------

void Shim::postMessage(String^ msg)
{
	client->postMessageRQ(sysStrToStdStr(msg));
}
//----< retrieve message from channel >----------------------------------

String^ Shim::getMessage()
{
	return stdStrToSysStr(client->getMessageSQ());
}

void Shim::startClient(String^ port)
{
	client->start(sysStrToStdStr(port));
}
#ifdef TEST_CLISHIM

int main()
{
  String^ msg = "Hello from sender";
  Shim^ pShim = gcnew Shim();
  std::cout << "\n  sending message \"" << pShim->sysStrToStdStr(msg) << "\"";
  pShim->PostMessage(msg);
  String^ pReply = pShim->GetMessage();
  std::cout << "\n  received message \"" << pShim->sysStrToStdStr(pReply) << "\"";
  std::cout << "\n\n";
}
#endif
