#pragma once
/////////////////////////////////////////////////////////////////////
// StrongCompAnal.h -  Identify Strong Componenets sets 		   //
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu          //
// CSE687 - OOD, Spring 2017									   //
/////////////////////////////////////////////////////////////////////
/*
Operation:
Using the DepTable created by DepAnalyzer and Graph class in Graph package
this package creates a directed graph and identifies the strong components

Public Interface:
StrongCompAnal sa(DepTable) //constructor saves the Dependencies Table
sa.doStrongCompAnal()       // Uses Graph and DepTable to create strongComps
sa.show()					// Prints contents of strongComps
sa.toXml(filename)			// Saves strongComps contents in Xml format

Required Files:
StrongCompAnal.h, StrongCompAnal.cpp
Graph.h, Graph.cpp
XmlDocument.h, XmlDocument.cpp
XmlElement.h, EmlElement.cpp

Build Command:
devenv TypeAnal.sln /rebuild debug

Maintanance History:
Ver 1.0 - March 7,2017 - Initial Release
*/
#include<unordered_map>
#include<string>

class StrongCompAnal
{
private:
	std::unordered_map <std::string, std::vector<std::string>> depTable;
	std::vector<std::string> files;
	std::vector<std::vector<std::string>> strongComps;
public:
	StrongCompAnal(std::unordered_map <std::string, std::vector<std::string>> depTable)
		: depTable(depTable)
	{
		for (auto item : depTable)
			files.push_back(item.first);
	};
	void doStrongCompAnal();
	void show();
	void toXml(std::string filename);
};

