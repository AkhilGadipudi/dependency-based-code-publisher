/////////////////////////////////////////////////////////////////////
// CodePublisher.cpp - Publishes code files as HTML pages and links //
//						HTML of dependencies						//
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu           //
// CSE687 - OOD, Spring 2017									    //
/////////////////////////////////////////////////////////////////////

#include"CodePublisher.h"
#include"../Utilities/Utilities.h"
#include<stack>
#include<string>
#include <windows.h>
#include <ShellAPI.h>
#include<iostream>
using namespace CodeAnalysis;
using namespace std;
// Processes command line arguments and runs CodeAnalyzer, TypeAnalyzer and DepAnalyzer
// Command line structure:
// Path for files to publish, followed by any number of file patters
// use -open filename.htm to open specific file in browser
void CodePublisher::processCommandLineArgs(int argc, char* argv[])
{
	std::vector<std::string> options;
	std::vector<std::string> patterns;
	std::cout << "\n Processing Command Line Arguments - Req #8";
	for (int i = 2; i < argc; i++)
	{
		if (std::string(argv[i]) == "-open")
			fileToOpen = argv[i + 1];
		if (std::string(argv[i])[0] == '/')
			options.push_back(argv[i]);
		else
			patterns.push_back(argv[i]);
	}
	CodeAnalysisExecutive exec;
	root = argv[1];
	// Run CodeAnalyzer on given file set
	exec.ProcessCommandLine(argc, argv);
	exec.showCommandLineArguments(argc, argv);
	exec.setDisplayModes();
	exec.getSourceFiles();
	std::cout << "\n Analysing Code to create AST";
	exec.processSourceCode(false);
	std::cout << "\n Code Analysis completed";

	// Start TypeAnalysis and get TypeTable
	TypeAnal ta;
	ta.doTypeAnal();
	tt = ta.getTypeTable();
	//tt.show();

	// Start Dependency analysis using TypeTable
	DepAnalyzer da(root, tt, patterns);
	da.doDepAnal();
	depTable = da.getDepTable();
	files = da.getFiles();
}

// Creates prolog for the file being published
std::string CodePublisher::getHtmlProlog(std::string file)
{
	return "<!------------------------------------------------------------------------\n" +
		file + ".htm - Publishes source code and dependencies of " + file + "\n" +
		"Akhil Gadipudi - OOD Spring 2017\n" +
		"-------------------------------------------------------------------------->\n";
}

// Helper funtion to reduce size of getPreTag, handles occurance of { and }
std::ostringstream handleBraces(std::string brace, bool insertButton, std::string nextTok = "")
{
	std::ostringstream out;
	static std::stack<char> scopes; // to identify scope of the hide/show code blocks
	static int idCount = 0;
	std::string buttonId, blockId;
	if (brace == "{") {
		if (insertButton) {
			scopes.push('y');
			buttonId = "b" + std::to_string(idCount); 
			blockId = "c" + std::to_string(idCount);
			idCount++;
			out << "<button id = \"" + buttonId + "\" value = \"#" + blockId + "\" " + "onclick = \'myToggle(id)\'>-</button> <span id = \"" + blockId + "\">";
			out << brace;
			insertButton = false;
		}
		else {
			out << brace;
			scopes.push('n');
		}
	}
	else if (brace == "}") {
		char topChar = scopes.top();
		if (topChar == 'y') {
			if (nextTok == ";") out << brace << nextTok << "</span>";
			else out << brace << "</span>" << nextTok;
		}
		else out << brace << nextTok;
		scopes.pop();
	}
	return out;
}
// Creates Pre tag to display scource code of the file being published
std::ostringstream CodePublisher::getPreTag(std::string file)
{
	std::ostringstream out;
	bool insertButton = false;
	std::ifstream is(file);
	Scanner::Toker toker;
	toker.returnComments(true);
	toker.addSplTok("\t");
	toker.addSplTok(" ");
	toker.attach(&is);
	out << "<pre class = \"indent\">\n";
	do 	// Process each token of the source file to create html
	{
		std::string tok = toker.getTok();
		if (tok == "<") out << "&lt;"; 
		else if (tok == "<<") out << "&lt;&lt;";
		else if (tok == ">") out << "&gt;";
		else if (tok == ">>") out << "&gt;&gt;";
		else if (tok == "{") out << handleBraces(tok, insertButton).str();
		else if (tok == "}") out << handleBraces(tok, insertButton, toker.getTok()).str();
		else {
			std::vector<TypeInfo> tiValues = tt.getValues(tok);
			if (tok == "main") insertButton = true;
			if (tiValues.size() > 0)
				for (TypeInfo ti : tiValues)
					if (ti.type == "namespace" || ti.type == "class" || ti.type == "function" || ti.type == "struct" || ti.type == "lambda" )
						insertButton = true;
			out << tok;
		}
	} while (is.good());
	out << "</pre>\n";
	is.close();
	return out;
}

// Creates Html for linking the dependencies
std::ostringstream CodePublisher::getDepsHtml(std::string file)
{
	std::ostringstream out;
	std::vector<std::string> deps = depTable[file];
	out << "<div class = \"indent\">\n" << "	<h4>Dependencies:</h4>\n";
	for (auto dep : deps)
		out << "		<a href = \"" + dep + ".htm\">" + dep + "</a>\n";
	out << "</div>\n";
	return out;
}

// creates html file for each file to be published
void CodePublisher::publish()
{
	
	std::string publishDir = Path::getFullFileSpec("../PublisherStorage/PublishedFiles/");
	std::cout << "\nCreating htm files at " + publishDir << std::endl;
	for (auto file : files)
	{		
		std::string htmFile = publishDir + Path::getName(file) + ".htm";
		std::ofstream os(htmFile);
		os << getHtmlProlog(Path::getName(file));
		os << "<html>\n";
		os << "<head>\n";
		os << " <link rel=\"stylesheet\" type=\"text/css\" href=\"styles/myStyle.css\">\n";
		os << " <script type=\"text/javascript\" src=\"scripts/jquery.js\"></script>\n";
		os << " <script type=\"text/javascript\" src=\"scripts/myScript.js\"></script>\n";
		os << "</head>\n";
		os << "<body>\n";
		os << "<h3>" + Path::getName(file) + "</h3>\n";
		if (indexCreated)
			os << "<a href = \"Index.htm\">[Back to Index]</a><br/>\n";
		os << "<hr />\n";
		os << getDepsHtml(Path::getName(file)).str();
		os << "<hr />\n";
		os << "<b><h4>Source Code:</h4></b>\n";
		os << getPreTag(file).str();
		os << "<hr />\n";
		os << "</body>\n";
		os << "</html>\n";
		os.close();
	}
}

// Creates Index of the published files
// Must be run before publish(), if not the published files wont have link to the Index page
void CodePublisher::createIndex()
{
	std::string indexHtml = Path::getFullFileSpec("../PublisherStorage/PublishedFiles/Index.htm");
	std::ofstream os(indexHtml);
	os << "<html>\n";
	os << "<head>\n";
	os << " <link rel=\"stylesheet\" type=\"text/css\" href=\"styles/myStyle.css\">\n";
	os << "</head>\n";
	os << "<body>\n";
	os << "<h1>CodePublisher Index</h1>\n";
	os << "<hr />\n";
	os << "<div class = \"indent\">";
	for (auto file : files)
		os << "		<a href = \"" + Path::getName(file) + ".htm\">" + Path::getName(file) + "</a><br/>\n";
	os << "</div>\n";
	os << "<hr />\n";
	os << "</body>\n</html>\n";
	indexCreated = true;
}

// Opens specfied file, opens Index.htm when file name not specified in command line
void CodePublisher::openHtml()
{
	std::string file = "Index.htm";
	if (fileToOpen != "")
		file = "../PublishedFiles/" + fileToOpen;
	std::string path = FileSystem::Path::getFullFileSpec("../PublishedFiles/" + file);
	std::wstring sPath(path.begin(), path.end());
	ShellExecute(NULL, L"open",(LPCWSTR)sPath.c_str(), NULL, NULL, SW_SHOWNORMAL);
}




#ifdef TEST_PUBLISHER
int main(int argc, char* argv[])
{
	::SetConsoleTitle(L"Publisher");
	CodePublisher cp;
	cp.processCommandLineArgs(argc, argv);
	cp.createIndex();
	cp.publish();
	cp.openHtml();
}
#endif // TEST_PUBLISHER
