#pragma once
/////////////////////////////////////////////////////////////////////
// CodePublisher.cpp - Publishes code files as HTML pages and links //
//						HTML of dependencies						//
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu           //
// CSE687 - OOD, Spring 2017									    //
/////////////////////////////////////////////////////////////////////
/*
Operation :
Creates HTML files for each file in the given set, the HTML pages shows the source code
with facility to fold code segments like namespaces, classes ans functions.
Each HTML page also provides links to the HTML pages of file(s) on which the current file
depends on.

Public Interface :
CodePublisher cp;
cp.processCommandLineArgs(argc, argv) //Process command line arguments

cp.createIndex() // Creates index with links to all published files
				// Must be called before publish(), if not it will still work fine
				// but the published files wont have link back to the index page
				// You can choose not to call this funtion if Index is not needed

cp.publish() //	Creates HTML files for each file in the file set

cp.openHtml() // Opens file specified in command line args, if not spcified opens Index.htm
			// You can choose not to call this funtion if you dont want to open any file

Required Files :
CodePublisher.h, CodePublisher.cpp
Executive.h, Executive.cpp
TypeTable.h, TypeTable.cpp
TypeAnal.h, TypeAnal.cpp
DepAnal.h, DepAnal.cpp
Tokenizer.h, Tokenizer.cpp

Build Command:
devenv CodePublisher.sln /rebuild debug

Maintenance History:
ver 1.0 - April 2, 2017 - Initial release
*/

#include"../TypeTable/TypeTable.h"
#include"../Analyzer/Executive.h"
#include"../TypeAnal/TypeAnal.h"
#include"../DepAnalyzer/DepAnalyzer.h"
#include"../Tokenizer/Tokenizer.h"
#include"../Cpp11-BlockingQueue/Cpp11-BlockingQueue.h"
#include<string>
#include<vector>
#include<unordered_map>
#include<sstream>


class CodePublisher
{
public:
	void processCommandLineArgs(int argc, char* argv[]);
	void publish();
	void createIndex();
	void openHtml();
private:
	bool indexCreated = false;
	std::string fileToOpen = "";
	std::string root;
	std::vector<std::string> files;
	TypeTable tt;
	std::unordered_map<std::string, std::vector<std::string>> depTable;
	std::ostringstream getPreTag(std::string file);
	std::ostringstream getDepsHtml(std::string file);
	std::string getHtmlProlog(std::string file);	
};