#include<iostream>
#include<string>
#include"Queries.h"


using namespace std;
void print_res(vector<string> res)
{
	for (string r : res)
	{
		cout << r << endl;
	}
	cout << endl;
}

#ifdef TEST_QUERIES
int main()
{
	NoSqlDb<string> db = NoSqlDb<string>("../StrDB.xml");
	cout << "=======================================================================================\n";
	cout << "Queries Demonstration\n";
	Query<string> q;
	using Res = vector<string>;
	cout << "Query : Value with key \"Queries.h\"\nResult :\n";
	cout << q.getValue(db, "Queries.h").show();
	cout << "\nQuery : Children of \"NoSqlDb.h\"\nResult (R1):\n";
	Res R1 = q.getChildren(db, "NoSqlDb.h");
	print_res(R1);
	cout << "Query : Keys containing the string\"Xml\"\nResult (R2):\n";
	Res R2 = q.getKeys(db, "Xml");
	print_res(R2);
	cout << "Query : Keys of elements that contain \"cpp\" in their name\nResult (R3):\n";
	Res R3 = q.KeysByName(db, "cpp");
	print_res(R3);
	cout << "Query : Keys of elements that contain \"Header file\" in their category\nResult (R4):\n";
	Res R4 = q.KeysByCategory(db, "Header file");
	print_res(R4);
	cout << "Query : Keys of elements that contain the string \"Documenet\" in their data\nResult (R5):\n";
	Res R5 = q.KeysByData(db, "Document");
	print_res(R5);
	cout << "Query : Keys of elements written between 2017-02-07 07:00:00 and " << timeString() << "\nResult (R6):\n";
	Res R6 = q.KeysByTimeIntv(db, "2017-02-07 07:00:00");
	print_res(R6);
	cout << "\n===   Compound Queries   =====\n";
	cout << "Query : Get Header files that contain Xml in name i.e filter R4 using the string Xml\nResult:\n";
	Res R7 = q.getKeys(db, "Xml", R4);
	print_res(R7);
	cout << "Query : Uninon of the results R3 and R4\nResult:\n";
	Res R8 = q.Union(R3, R4);
	print_res(R8);
}
#endif