#pragma once
/////////////////////////////////////////////////////////////////////
// Queries.h - Provides funtions to perform queries on a data base //
// Author : Akhil Gadipudi		 SU email : agadipud@syr.edu 	   //
/////////////////////////////////////////////////////////////////////
/*
* Package information:
* --------------------
* This package provides various queries that can be performed on the 
* No Sql Database created using "NoSqlDb.h"
*
* Public interface:
* -----------------
* getValue - returns value with a specified key
* getChildren - return vector of key of a children of specified key
* KeysByName - return keys in data base or from list provided keys 
*	with name contains specified string
* KeysByCategory - return keys in data base or from list provided keys
*	with category contains specified string
* KeysByData - return keys in data base or from list provided keys
*	with data contains specified string, only works when data is comparable to string
* KeysByTimeInvl - return keys in database or from list of provided keys
*   which are written between specified time intervel or  between given time to current time
* Union - performs uninon operation on 2 sets of keys
*
* Required files:
* ---------------
* Queries.h, Queris.cpp
* NoSqlDb.h, NoSqlDb.cpp
* StrHelper.h, StrHelper.cpp
* 
* Build Process:
* --------------
*  devenv Queries.sln /debug rebuild
*
* Maintenance History:
* -------------------
* ver 1.0 : 2/7/2017
*    - First release
*/

#include<string>
#include<vector>
#include<algorithm>
#include<iterator>
#include "../NoSqlDb/NoSqlDb.h"
#include "../StrHelper/StrHelper.h"

using Key = std::string;
using Keys = std::vector<std::string>;

template<typename Data>
class Query
{
public:
	Element<Data> getValue(NoSqlDb<Data>& db, Key key);
	Keys getChildren(NoSqlDb<Data>& db, Key key);
	Keys getKeys(NoSqlDb<Data>& db, std::string pattern = "", Keys keys = Keys());
	Keys KeysByName(NoSqlDb<Data>& db, std::string pattern, Keys keys = Keys());
	Keys KeysByCategory(NoSqlDb<Data>& db, std::string pattern, Keys keys = Keys());
	Keys KeysByData(NoSqlDb<Data>& db, std::string pattern, Keys keys = Keys());
	Keys KeysByTimeIntv(NoSqlDb<Data>& db, std::string fromTime, std::string toTime = timeString(), 
		Keys keys = Keys());
	Keys Union(Keys k1, Keys k2);
private:

};

// returns values in db with specified key
template<typename Data>
Element<Data> Query<Data>::getValue(NoSqlDb<Data>& db, Key key)
{ 
	return db.value(key);
}

// get children of a element in db
template<typename Data>
Keys Query<Data>::getChildren(NoSqlDb<Data>& db, Key key)
{
	Element<Data> elem = db.value(key);
	return elem.getChildren();
}

// gets all keys by default, or filter given list of keys by pattern
template<typename Data>
Keys Query<Data>::getKeys(NoSqlDb<Data>& db, std::string pattern = "",Keys keys = Keys() )
{
	Keys allKeys = (keys.size() == 0) ? db.getKeys() : keys;
	if (pattern == "")
		return allKeys;
	Keys resKeys;
	for (Key key : allKeys)
	{
		//std::size_t found = (key.find(pattern));
		//if(found != std::string::npos)	
		//	resKeys.push_back(key);
		regex e(pattern);
		if(regex_search(key, e))
			resKeys.push_back(key);
	}
	return resKeys;
}

// KeysByName - return keys in data base or from list provided keys 
// with name contains specified string
template<typename Data>
Keys Query<Data>::KeysByName(NoSqlDb<Data>& db, std::string pattern, Keys keys = Keys())
{
	Keys resKeys;
	Keys allKeys = (keys.size() == 0) ? db.getKeys() : keys;
	for (Key key : allKeys)
	{
		Element<Data> elem = db.value(key);
		std::string temp = elem.name;
		//std::size_t found = (temp.find(pattern));
		//if (found != std::string::npos)
		//	resKeys.push_back(key);
		regex e(pattern);
		if (regex_search(temp, e))
			resKeys.push_back(key);
	}
	return resKeys;
}

// KeysByCategory - return keys in data base or from list provided keys
//	with category contains specified string
template<typename Data>
Keys Query<Data>::KeysByCategory(NoSqlDb<Data>& db, std::string pattern, Keys keys = Keys())
{
	Keys resKeys;
	Keys allKeys = (keys.size() == 0) ? db.getKeys() : keys;
	for (Key key : allKeys)
	{
		Element<Data> elem = db.value(key);
		std::string temp = elem.category;
		//std::size_t found = (temp.find(pattern));
		//if (found != std::string::npos)
		//	resKeys.push_back(key);
		regex e(pattern);
		if (regex_search(temp, e))
			resKeys.push_back(key);
	}
	return resKeys;
}

// KeysByData - return keys in data base or from list provided keys
//	with data contains specified string, only works when data is comparable to string
template<typename Data>
Keys Query<Data>::KeysByData(NoSqlDb<Data>& db, std::string pattern, Keys keys = Keys())
{
	Keys resKeys;
	Keys allKeys = (keys.size() == 0) ? db.getKeys() : keys;
	if (typeid(Data) != typeid(std::string))
	{
		std::cout << "Data type not string\n";
		return Keys();
	}
	for (Key key : allKeys)
	{
		Element<Data> elem = db.value(key);
		Data temp = elem.data;
		//std::size_t found = (temp.find(pattern));
		//if (found != std::string::npos)
		//	resKeys.push_back(key);
		regex e(pattern);
		if (regex_search(temp, e))
			resKeys.push_back(key);
	}
	return resKeys;
}
// KeysByTimeInvl - return keys in database or from list of provided keys
//   which are written between specified time intervel or between given time to current time

template<typename Data>
Keys Query<Data>::KeysByTimeIntv(NoSqlDb<Data>& db, std::string fromTime,
	std::string toTime = timeString(), Keys keys = Keys())
{
	Keys resKeys;
	Keys allKeys = (keys.size() == 0) ? db.getKeys() : keys;
	for (Key key : allKeys)
	{
		Element<Data> elem = db.value(key);
		std::string temp = elem.timeDate;
		if (temp >= fromTime && temp <= toTime)
			resKeys.push_back(key);
	}
	return resKeys;
}

// Union - performs uninon operation on 2 sets of keys
template<typename Data>
Keys Query<Data>::Union(Keys k1, Keys k2)
{
	Keys res;
	std::sort(k1.begin(), k1.end());
	std::sort(k2.begin(), k2.end());
	std::set_union(k1.begin(), k1.end(), k2.begin(), k2.end(), std::back_inserter(res));
	return res;
}