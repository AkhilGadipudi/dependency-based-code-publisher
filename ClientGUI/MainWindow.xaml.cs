﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using forms = System.Windows.Forms;
using controls = System.Windows.Controls;
namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public class Message
    {
        public Message()
        {
           
        }
        public Message(string s)
        {
            string[] pairs = s.Split(';');
            foreach (string pair in pairs)
            {
                if (pair == "")
                    continue;
                string[] pairItems = pair.Split('^');
                this.add(pairItems[0], pairItems[1]);
            }
        }
        private Dictionary<string, string> items = new Dictionary<string, string>();
        public void add(string key, string value)
        {
            items.Add(key, value);
        }
        public void remove(string key)
        {
            items.Remove(key);
        }
        public string get(string key)
        {
            return items[key];  
        }
        public string toString()
        {
            string s = "";
            foreach (string key in items.Keys)
            {
                s += key + "^" + items[key] + ";";
            }
            return s;
        }
    }

    public partial class MainWindow : Window
    {
        Shim shim = new Shim();
        Thread tRcv;
        string serverip = "";
        string serverport = "";
        string clientport = Environment.GetCommandLineArgs()[1];
        
        public MainWindow()
        {
            InitializeComponent();        
            ActivityBox.Items.Insert(0, "Activity :");
            shim.startClient(clientport);
            addActivity("Client port set to : " + clientport);
            FilesList.SelectionMode = controls.SelectionMode.Extended;
        }
        ~MainWindow() { System.Windows.Forms.Application.Exit(); }
        private void button_click(object sender, RoutedEventArgs e) {
           
        }

        private void addActivity(string s)
        {
            ActivityBox.Items.Insert(1, s);
        }
        
        private void textbox_focused(object sender, RoutedEventArgs e)
        {
            controls.TextBox box = sender as controls.TextBox;
            box.Text = "";
        }
        private void connect_button(object sender, RoutedEventArgs e)
        {
            serverip = server_ip_box.Text;
            serverport = server_port_box.Text;
            Message msg = new Message();
            msg.add("type", "connect");
            msg.add("serverip", serverip);
            msg.add("serverport", serverport);
            addActivity("Trying to connect to " + serverip + ":" + serverport);
            shim.postMessage(msg.toString());

            Message msg2 = new Message();
            msg2.add("type", "dloadfiles");
            msg2.add("serverip", serverip);
            msg2.add("serverport", serverport);
            string files = "../PublisherStorage/PublishedFiles/scripts/myScript.js," +
                            "../PublisherStorage/PublishedFiles/scripts/jquery.js," +
                            "../PublisherStorage/PublishedFiles/styles/myStyle.css,";
            msg2.add("files", files);
            addActivity("Downloading scripts and styles");
            shim.postMessage(msg2.toString());
        }
        private void brs_button_dir_ul(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            DialogResult dr = fd.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                dirForUl.Items.Clear();
                string[] files = System.IO.Directory.GetFiles(fd.SelectedPath, "*", System.IO.SearchOption.AllDirectories);
                foreach (string file in files)
                    dirForUl.Items.Insert(0, file);
            }
        }

        private void upld_button_dir_ul(object sender, RoutedEventArgs e)
        {
            string files = "";
            for (int i = 0; i < dirForUl.Items.Count; i++)
                files += (string)dirForUl.Items[i] + ",";
            Message msg = new Message();
            msg.add("type", "uploadfiles");
            msg.add("fileslist", files);
            msg.add("serverip", serverip);
            msg.add("serverport", serverport);
            if (dirCategory.Text == "Enter Category" || dirCategory.Text == "")
            {
                addActivity("Cannot upload without category");
                return;
            }
            msg.add("category", dirCategory.Text);
            shim.postMessage(msg.toString());
            addActivity("Uploading files");
        }

        private void brs_button_file_ul(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.Multiselect = true;
            DialogResult dr = fd.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                fileForUl.Items.Clear();
                foreach (string file in fd.FileNames)
                    fileForUl.Items.Insert(0, file);                   
            }            
        }
        private void upld_button_file_ul(object sender, RoutedEventArgs e)
        {
            string files = "";
            for (int i = 0; i <fileForUl.Items.Count; i++)
                files += (string)fileForUl.Items[i] + ",";
            Message msg = new Message();
            msg.add("type", "uploadfiles");
            msg.add("fileslist", files);
            msg.add("serverip", serverip);
            msg.add("serverport", serverport);
            if (fileCategory.Text == "Enter Category" || fileCategory.Text == "")
            {
                addActivity("Cannot upload without category");
                return;
            }
            msg.add("category", fileCategory.Text);
            shim.postMessage(msg.toString());
            addActivity("Uploading files");
        }

        private void publish_button(object sender, RoutedEventArgs e)
        {
            string publishDir = (string)PubDirList.SelectedItem;
            if (publishDir == "")
            {
                addActivity("Error : Directory must be selected to publish");
                return;
            }
            string patterns = publish_patterns.Text;
            if (patterns == "Patterns list" || patterns == "")
                patterns = "*";
            Message msg = new Message();
            msg.add("type", "publish");
            msg.add("dir", publishDir);
            msg.add("patterns", patterns);
            msg.add("serverip", serverip);
            msg.add("serverport", serverport);
            shim.postMessage(msg.toString());
            addActivity("Publishing files");
        }

        private void window_loaded(object sender, RoutedEventArgs e)
        {           
            tRcv = new Thread(() => {
                while (true)
                {
                    Message msg = new Message(shim.getMessage());
                    string msgType = msg.get("type");
                    if (msgType == "msg")
                    {
                        string strMsg = msg.get("msg");
                        Action<string> act = new Action<string>(addActivity);
                        Object[] args = { strMsg };
                        Dispatcher.Invoke(act, args);
                    }
                    else if (msgType == "dirs")
                    {
                        string[] newDirs = msg.get("dirs").Split(',');
                        Action<controls.ListBox, string[]> act = new Action<controls.ListBox, string[]>(updateListBox);
                        Object[] args = { DirList, newDirs };
                        Dispatcher.Invoke(act, args);
                        Object[] args2 = { PubDirList, newDirs };
                        Dispatcher.Invoke(act, args2);
                    }
                    else if (msgType == "fileslist")
                    {
                        string[] files = msg.get("fileslist").Split(',');
                        Action<controls.ListBox, string[]> act = new Action<controls.ListBox, string[]>(updateListBox);
                        Object[] args = { FilesList, files };
                        Dispatcher.Invoke(act, args);
                    }
                    else if (msgType == "pubfileslist")
                    {
                        string[] files = msg.get("pubfileslist").Split(',');
                        Action<controls.ListBox, string[]> act = new Action<controls.ListBox, string[]>(updateListBox);
                        Object[] args = { PubedFiles, files };
                        Dispatcher.Invoke(act, args);
                    }
                }
            }
            );
            tRcv.Start();
        }
      
        public void updateListBox(controls.ListBox lb, string[] newitems)
        {
            lb.Items.Clear();
            foreach (string newitem in newitems)
                if(newitem != "")
                    lb.Items.Insert(0, newitem);
        }

        private void DirList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string dirSelected = (string)DirList.SelectedItem;
            FilesList.Items.Clear();
            FilesList.Items.Insert(0, "Fetching files list");
            Message msg = new Message();
            msg.add("type", "reqfileslist");
            msg.add("dir", dirSelected);
            msg.add("serverip", serverip);
            msg.add("serverport", serverport);
            shim.postMessage(msg.toString());
        }

        private void refresh_dirs(object sender, RoutedEventArgs e)
        {
            DirList.Items.Clear();
            DirList.Items.Insert(0, "Fetching Directories");
            PubDirList.Items.Clear();
            PubDirList.Items.Insert(0, "Fetching Directories");
            Message msg = new Message();
            msg.add("type", "dirs");
            msg.add("serverip", serverip);
            msg.add("serverport", serverport);
            shim.postMessage(msg.toString());
            addActivity("Directories updated");
        }

        private void dload_files(object sender, RoutedEventArgs e)
        {           
            Message msg = new Message();
            msg.add("type", "dloadfiles");
            msg.add("serverip", serverip);
            msg.add("serverport", serverport);
            string files = "";
            foreach (string file in FilesList.SelectedItems)
            {
                files += file + ",";
            }
            msg.add("files", files);
            if (FilesList.SelectedItems.Count == 0)
            {
                addActivity("Files not seleted");
                return;
            }
                shim.postMessage(msg.toString());
                addActivity("Downloading files");
        }

        private void delete_files(object sender, RoutedEventArgs e)
        {
            Message msg = new Message();
            msg.add("type", "dltfiles");
            msg.add("serverip", serverip);
            msg.add("serverport", serverport);
            string files = "";
            foreach (string file in FilesList.SelectedItems)
            {
                files += file + ",";
            }
            msg.add("files", files);
            if (FilesList.SelectedItems.Count == 0)
            {
                addActivity("Files not seleted");
                return;
            }
            FilesList.Items.Clear();
            FilesList.Items.Insert(0, "Refresh and Re-select dir to see files");
            shim.postMessage(msg.toString());
            addActivity("Deleting files");
        }

        private void pub_files_rfsh_button(object sender, RoutedEventArgs e)
        {
            Message msg = new Message();
            msg.add("type", "pubfileslist");
            shim.postMessage(msg.toString());
        }

        private void openfile(object sender, RoutedEventArgs e)
        {
            string file = PubedFiles.SelectedItem.ToString();
            string path = System.IO.Path.GetFullPath("../ClientStorage/" + file);
            Process.Start(path);
        }
    }
}
