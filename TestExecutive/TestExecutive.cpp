/////////////////////////////////////////////////////////////////////
// TestExecutive.cpp -  Demonstrates use of Remote CodePublisher	//
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu          //
// CSE687 - OOD, Spring 2017									   //
/////////////////////////////////////////////////////////////////////
#define IN_DLL
#include <string>
#include <iostream>
#include<vector>
#include<thread>
#include<unordered_map>
#include"../Client/IClient.h"
#include"../FileSystem/FileSystem.h"

using namespace std;
using Map = unordered_map<string, string>;
vector<string> splitstrte(string s, string delim)
{
	vector<string> toks;
	auto start = 0U;
	auto end = s.find(delim);
	while (end != std::string::npos)
	{
		toks.push_back(s.substr(start, end - start));
		start = end + delim.length();
		end = s.find(delim, start);
	}
	toks.push_back(s.substr(start, end));
	return toks;
}
Map strToMap(string msg)
{
	unordered_map<string, string> map;
	vector<string> pairs = splitstrte(msg, ";");
	for (string pair : pairs)
	{
		if (pair == "")
			continue;
		vector<string> pairItems = splitstrte(pair, "^");
		string key = pairItems[0];
		string value = pairItems[1];
		map[key] = value;
	}
	return map;
}
string maptoStrTe(Map map)
{
	string s = "";
	for (auto pair : map)
	{
		s += pair.first + "^" + pair.second + ";";
	}
	return s;
}

string postFile(string ip, string port)
{
	Map map;
	cout << "uploading few files from \"files\"(local folder) into a folder \"test\" in publisher - REQ #5 REQ #8" <<endl;
	map["type"] = "uploadfiles";
	map["fileslist"] = "../files/Parent.h,../files/Parent.cpp,../files/Grandparent.h,../files/Test.cpp,";
	map["serverip"] = ip;
	map["serverport"] = port;
	map["category"] = "test";
	return maptoStrTe(map);
}

string getdirs(string ip, string port)
{
	Map map;
	cout << "Reqesting dirs list from testExec" << endl;
	map["type"] = "dirs";
	map["serverip"] = ip;
	map["serverport"] = port;
	return maptoStrTe(map);
}

string getfileslist(string ip, string port)
{
	Map map;
	cout << "Requesting list of files in dir \"PublisherStorage/test\"" << endl;
	map["type"] = "reqfileslist";
	map["dirs"] = "../PublisherStorage/test";
	map["serverip"] = ip;
	map["serverport"] = port;
	return maptoStrTe(map);
}

string dltfiles(string ip, string port)
{
	Map map;
	cout << "Requesting to delete file \"PublisherStorage/test/Test.cpp\"" << endl;
	map["type"] = "dltfiles";
	map["files"] = "../PublisherStorage/test/Test.cpp,";
	map["serverip"] = ip;
	map["serverport"] = port;
	return maptoStrTe(map);
}

string dloadfiles(string ip, string port)
{
	Map map;
	cout << "Requesting to download file \"PublisherStorage/PublishedFiles/scripts/myScript.js\"" << endl;
	map["type"] = "dloadfiles";
	map["files"] = "../PublisherStorage/PublishedFiles/scripts/myScript.js,";
	map["serverip"] = ip;
	map["serverport"] = port;
	return maptoStrTe(map);
}

string publish(string ip, string port)
{
	Map map;
	cout << "Requesting to Publish files in \"PublisherStorage/files\"" << endl;
	map["type"] = "publish";
	map["dir"] = "../PublisherStorage/files";
	map["patterns"] = "*.*,";
	map["serverip"] = ip;
	map["serverport"] = port;
	return maptoStrTe(map);
}

int main(int argc, char* argv[])
{
	FileSystem::Directory::setCurrentDirectory("../../ClientGUI/bin/Debug");
	ObjectFactory f;
	string serverip = argv[1];
	string serverport = argv[2];
	string clientport = argv[3];
	IClient* c = f.createClient();
	c->start(clientport);
	cout << "TextExec - simulates gui funtinality \n"
		<< "Sends predefined msgs to client to demonstrate all functions\n"
		<< "Uses functions used in CLIShim to work similar to GUI\n" << endl;

	cout << "Done using Visual Studio 2015 and its C++ Windows console projects and WPF for GUI  - REQ #1\n";
	cout << "Used C++ standard library's streams for all console I/O and new and delete for all heap-based memory management - REQ #2\n";
	cout << "Provide Server program that provides functionality to publish, the contents of a set of C++ source code files\n";
	cout << "Saticified Proj3 requirments - REQ #4\n";
	cout << "All commmunication between server and client is done as HttpMessages - REQ #7\n";


	thread t([&]()
	{
		while (true)
		{
			string msg = c->getMessageSQ();
			Map map = strToMap(msg);
			if (map["type"] == "dirs")
			{
				cout << "Received dirs list :\n";
				vector<string> dirs = splitstrte(map["dirs"], ",");
				for (string dir : dirs)
					if (dir != "")
						cout << " " + dir << endl;
				cout << endl;
			}
			else if (map["type"] == "fileslist")
			{
				cout << "Received files list :\n";
				vector<string> files = splitstrte(map["fileslist"], ",");
				for (string file : files)
					if (file != "")
						cout << " " + file << endl;
				cout << endl;
			}
		}
	}
	);
	t.detach();

	//The sleeps are only used to give time gap between the request so the 
	// text written by client and testexecutive to console dont get mixed up
	c->postMessageRQ(postFile(serverip, serverport));
	::Sleep(2000);
	c->postMessageRQ(getdirs(serverip, serverport));
	::Sleep(2000);
	c->postMessageRQ(getfileslist(serverip, serverport));
	::Sleep(2000);
	c->postMessageRQ(dltfiles(serverip, serverport));
	::Sleep(2000);
	c->postMessageRQ(getfileslist(serverip, serverport));
	::Sleep(2000);
	c->postMessageRQ(dloadfiles(serverip, serverport));
	::Sleep(2000);
	c->postMessageRQ(publish(serverip, serverport));
	cin.get();

	cout << "Test Executive demostrate all requirments - REQ #9" << endl;
	return 0;
}
