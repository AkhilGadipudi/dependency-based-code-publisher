#define IN_DLL
/////////////////////////////////////////////////////////////////////
// Client.h - Used to communicate with remote publisher				//
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu          //
// CSE687 - OOD, Spring 2017									   //
/////////////////////////////////////////////////////////////////////
#include<iostream>
#include"Client.h"
#include"../FileSystem/FileSystem.h"
#include"../Utilities/Utilities.h"
#include<unordered_map>
using namespace std;

// creates HttpMessages to send to server
HttpMessage Client::makeMessage(string postType, const std::string& msgBody, const EndPoint& ep)
{
	HttpMessage msg;
	msg.clear();
	msg.addAttribute(HttpMessage::attribute("POST", postType));
	msg.addAttribute(HttpMessage::attribute("to" , ep));
	msg.addAttribute(HttpMessage::attribute("from" , myEndPoint));
	if (msgBody.size() > 0)
	{
		msg.addBody(msgBody);
		msg.addAttribute(HttpMessage::attribute("content-length", std::to_string(msgBody.size())));
	}
	return msg;
}

// Sends a Httpmessage to server through already connected socket
void Client::sendMessage(HttpMessage& msg, Socket& socket)
{
	std::string msgString = msg.toString();
	socket.send(msgString.size(), (Socket::byte*)msgString.c_str());
}

// Sends file to a server though already connected socket
bool Client::sendFile(const std::string& fqname, const std::string& category,const EndPoint& ep, Socket& socket)
{

	FileSystem::FileInfo fi(fqname);
	size_t fileSize = fi.size();
	std::string sizeString = Utilities::Converter<size_t>::toString(fileSize);
	FileSystem::File file(fqname);
	file.open(FileSystem::File::in, FileSystem::File::binary);
	if (!file.isGood())
		return false;

	HttpMessage msg = makeMessage("File", "", ep);
	msg.addAttribute(HttpMessage::Attribute("filename", FileSystem::Path::getName(fqname)));
	msg.addAttribute(HttpMessage::Attribute("content-length", sizeString));
	msg.addAttribute(HttpMessage::Attribute("category", category));
	sendMessage(msg, socket);
	const size_t BlockSize = 2048;
	Socket::byte buffer[BlockSize];
	while (true)
	{
		FileSystem::Block blk = file.getBlock(BlockSize);
		if (blk.size() == 0)
			break;
		for (size_t i = 0; i < blk.size(); ++i)
			buffer[i] = blk[i];
		socket.send(blk.size(), buffer);
		if (!file.isGood())
			break;
	}
	file.close();
	return true;
}

// Used as funtor/Callable object by socketlistner provided in sockets package
void ClientReceiver::operator()(Socket socket)
{
	while (true)
	{
		HttpMessage msg = readMessage(socket);
		if (connectionClosed_ || msg.bodyString() == "QUIT")
		{
			//std::cout << "Clients's Receiver Stopped" << std::endl;
			break;
		}
		msgQ_.enQ(msg);
	}
}

// Reads message from socket and creates httpmessage
HttpMessage ClientReceiver::readMessage(Socket& socket)
{
	connectionClosed_ = false;
	HttpMessage msg;
	while (true)
	{
		std::string attribString = socket.recvString('\n');
		if (attribString.size() > 1)
		{
			HttpMessage::Attribute attrib = HttpMessage::parseAttribute(attribString);
			msg.addAttribute(attrib);
		}
		else
		{
			break;
		}
	}
	// If client is done, connection breaks and recvString returns empty string
	if (msg.attributes().size() == 0)
	{
		connectionClosed_ = true;
		return msg;
	}
	// read body if POST
	std::string postType = msg.findValue("POST");
	if (postType == "File")
	{
		bool success = readFile(msg, socket);
		if (success)
			msg.addAttribute(HttpMessage::Attribute("copyStatus", "Sucess"));
		else
			msg.addAttribute(HttpMessage::Attribute("copyStatus", "Fail"));
	}
	else
	{
		// read message body
		size_t numBytes = 0;
		size_t pos = msg.findAttribute("content-length");
		if (pos < msg.attributes().size())
		{
			numBytes = Utilities::Converter<size_t>::toValue(msg.attributes()[pos].second);
			Socket::byte* buffer = new Socket::byte[numBytes + 1];
			socket.recv(numBytes, buffer);
			buffer[numBytes] = '\0';
			std::string msgBody(buffer);
			msg.addBody(msgBody);
			delete[] buffer;
		}
	}
	return msg;
}

// Reads frile from socket, called after reciving a httpmessage specifying a file is being sent as stream
bool ClientReceiver::readFile(HttpMessage& msg, Socket& socket)
{
	size_t contentSize;
	std::string filename = msg.findValue("filename");
	std::string category = msg.findValue("category");
	std::string sizeString = msg.findValue("content-length");
	if (sizeString != "") contentSize = Utilities::Converter<size_t>::toValue(sizeString);
	else return false;
	std::string fqname;
	if (category == "NULL" || category == "")
		fqname = FileSystem::Path::getFullFileSpec("../ClientStorage/" + filename);
	else {
		FileSystem::Directory::create(FileSystem::Path::getFullFileSpec("../ClientStorage/" + category));
		fqname = FileSystem::Path::getFullFileSpec("../ClientStorage/" + category + "/" + filename);
	}
	FileSystem::File file(fqname);
	file.open(FileSystem::File::out, FileSystem::File::binary);
	const size_t BlockSize = 2048;
	Socket::byte buffer[BlockSize];
	size_t bytesToRead;
	FileSystem::Block blk;
	if (!file.isGood())
	{
		std::cout << " can't open file " + fqname<<endl;
		while (true)
		{
			if (contentSize > BlockSize) bytesToRead = BlockSize;
			else bytesToRead = contentSize;
			socket.recv(bytesToRead, buffer);
			for (size_t i = 0; i < bytesToRead; ++i) blk.push_back(buffer[i]);
			if (contentSize < BlockSize) break;
			contentSize -= BlockSize;
		}
		return false;
	}
	while (true)
	{
		if (contentSize > BlockSize) bytesToRead = BlockSize;
		else bytesToRead = contentSize;
		socket.recv(bytesToRead, buffer);
		for (size_t i = 0; i < bytesToRead; ++i) blk.push_back(buffer[i]);
		file.putBlock(blk);
		if (contentSize < BlockSize) break;
		contentSize -= BlockSize;
	}
	file.close();
	return true;
}

// used convert a map into string for communication with gui
string mapToStr(unordered_map<string, string> map)
{
	string s = "";
	for (auto pair : map)
	{
		s += pair.first + "^" + pair.second + ";";
	}
	return s;
}

// Function run on a thread to constantly listen for messages from gui
void GuiComm::clientMsgHandler(Async::BlockingQueue<HttpMessage>& msgQ)
{
	while (true)
	{
		HttpMessage msg = msgQ.deQ();		
		string postType = msg.findValue("POST");
		cout << "Client recvd msg from server : " + postType << endl;
		if (msg.bodyString() == "QUIT")
			break;
		else if (postType == "MSG")
			cout << "Client recvd msg from server : " + msg.bodyString() << endl;
		else if (postType == "File")
		{
			string copyStatus = msg.findValue("copyStatus");
			if (copyStatus == "Fail")
				cout << "Failed to copy " + msg.findValue("filename") << endl;
			else
				cout << "Copied file " + msg.findValue("filename") << endl;
		}
		else if (postType == "Dirs")
		{
			unordered_map<string, string> map;
			map["type"] = "dirs";
			map["dirs"] = msg.bodyString();
			this->postMessageSQ(mapToStr(map));
		}
		else if (postType == "FilesList")
		{
			unordered_map<string, string> map;
			map["type"] = "fileslist";
			map["fileslist"] = msg.bodyString();
			this->postMessageSQ(mapToStr(map));
		}
	}
}

// posts message in a queue containg messages to send from client
void GuiComm::postMessageSQ(std::string msg)
{
	sendQ.enQ(msg);
}
// used by gui to get message sent by client
std::string GuiComm::getMessageSQ()
{
	return sendQ.deQ();
}
// used by gui to post message to client
void GuiComm::postMessageRQ(std::string msg)
{
	recvQ.enQ(msg);
}
// used by client to get messages from gui
std::string GuiComm::getMessageRQ()
{
	return recvQ.deQ();
}
// used to create object of GuiComm as Iclient object for CLIshim to use methods provided through interface
IClient* ObjectFactory::createClient()
{
	return new GuiComm;
}

// used to split string using a spcific delimiter
vector<string> splitstr(string s, string delim)
{
	vector<string> toks;
	auto start = 0U;
	auto end = s.find(delim);
	while (end != std::string::npos)
	{
		toks.push_back(s.substr(start, end - start));
		start = end + delim.length();
		end = s.find(delim, start);
	}
	toks.push_back(s.substr(start, end));
	return toks;
}

// Creates a map from string message received from gui
unordered_map<string,string> GuiComm::parseGuiMsg(string msg)
{
	//cout << " Parsing Msg from GUI : " + msg << endl;
	unordered_map<string, string> map;
	vector<string> pairs = splitstr(msg, ";");
	for (string pair : pairs)
	{
		if (pair == "")
			continue;
		vector<string> pairItems = splitstr(pair, "^");
		string key = pairItems[0];
		string value = pairItems[1];
		map[key] = value;
	}
	return map;
}

// helper to upload files to server
void GuiComm::helper_uploadfiles(unordered_map<string, string> map)
{
	cout << "\nUploading files to server" << endl;
	vector<string> files = splitstr(map["fileslist"], ",");
	SocketSystem ss;
	SocketConnecter sc;
	string serverEp = map["serverip"] + map["serverport"];
	sc.connect(map["serverip"], Utilities::Converter<size_t>::toValue(map["serverport"]));
	for (string file : files) {
		if (client.sendFile(file, map["category"], serverEp, sc))
			cout << "File uploaded : " + file << endl;
		else
			if (file != "")
				cout << "Failed to upload : " + file << endl;
	}
	HttpMessage msg = client.makeMessage("DirsReq", "", serverEp);
	client.sendMessage(msg, sc);
}

// helper to download files from server
void GuiComm::helper_dloadfiles(unordered_map<string, string> map)
{
	cout << "\nRequesting server to send files" << endl;
	SocketSystem ss;
	SocketConnecter sc;
	string serverEp = map["serverip"] + map["serverport"];
	sc.connect(map["serverip"], Utilities::Converter<size_t>::toValue(map["serverport"]));
	HttpMessage msg = client.makeMessage("FileReq", map["files"], serverEp);
	client.sendMessage(msg, sc);
}

// helper to post publish request to server
void GuiComm::helper_publish(unordered_map<string, string> map)
{
	SocketSystem ss;
	SocketConnecter sc;
	string serverEp = map["serverip"] + map["serverport"];
	sc.connect(map["serverip"], Utilities::Converter<size_t>::toValue(map["serverport"]));
	string body = "NULL," + map["dir"] + "," + map["patterns"];
	HttpMessage msg = client.makeMessage("PublishCmd", body, serverEp);
	client.sendMessage(msg, sc);
	msg.clear();
	msg = client.makeMessage("DirsReq", "", serverEp);
	client.sendMessage(msg, sc);
	cout << "\nRequested server to publish files at : " + map["dir"] << endl;
}

// helper funtion to send gui list of published files
void GuiComm::helper_pubfileslist()
{
	cout << "\nGetting published files in local dir" << endl;
	string path = FileSystem::Path::getFullFileSpec("../ClientStorage/");
	vector<string> files = FileSystem::Directory::getFiles(path, "*.htm");
	string pubfileslist = "";
	for (string file : files)
		pubfileslist += file + ",";
	unordered_map<string, string> map;
	map["type"] = "pubfileslist";
	map["pubfileslist"] = pubfileslist;
	this->postMessageSQ(mapToStr(map));
}

// takes a map containing info from gui and process the request
void GuiComm::processGuiMsg(unordered_map<string, string> map)
{
	//cout << "\nProcessing message from Gui" << endl;
	string msgType = map["type"];
	if (msgType == "uploadfiles")
		helper_uploadfiles(map);
	else if (msgType == "reqfileslist")
	{
		SocketSystem ss;
		SocketConnecter sc;
		string serverEp = map["serverip"] + map["serverport"];
		sc.connect(map["serverip"], Utilities::Converter<size_t>::toValue(map["serverport"]));
		HttpMessage msg = client.makeMessage("FilesListReq", map["dir"], serverEp);
		if (map["dir"] != "") {
			client.sendMessage(msg, sc);
			cout << "Requesting files list for dir : " + map["dir"] << endl;
		}
	}
	else if (msgType == "connect" || msgType == "dirs")
	{
		SocketSystem ss;
		SocketConnecter sc;
		string serverEp = map["serverip"] + map["serverport"];
		cout << "Trying to connect to " + serverEp << endl;
		sc.connect(map["serverip"], Utilities::Converter<size_t>::toValue(map["serverport"]));
		HttpMessage msg = client.makeMessage("DirsReq", "", serverEp);
		client.sendMessage(msg, sc);
	}
	else if (msgType == "publish")
	{
		helper_publish(map);
	}
	else if (msgType == "dltfiles")
	{
		SocketSystem ss;
		SocketConnecter sc;
		string serverEp = map["serverip"] + map["serverport"];
		sc.connect(map["serverip"], Utilities::Converter<size_t>::toValue(map["serverport"]));
		HttpMessage msg = client.makeMessage("DltFilesReq", map["files"], serverEp);
		client.sendMessage(msg, sc);
		cout << "Requesting server to delete files" << endl;
	}
	else if (msgType == "dloadfiles")
		helper_dloadfiles(map);
	else if (msgType == "pubfileslist")
		helper_pubfileslist();
}
// used though IClient object by gui to start client listner threads
void GuiComm::start(string port)
{
	::SetConsoleTitle(L"Client");
	FileSystem::Directory::setCurrentDirectory("../../");
		thread t2([&]() {
		try {
			size_t port_num = Utilities::Converter<size_t>::toValue(port);
			client = Client(port);
			SocketSystem ss;
			SocketListener sl(port_num, Socket::IP6);
			Async::BlockingQueue<HttpMessage> msgQ;
			ClientReceiver ch(msgQ);
			sl.start(ch);
			cout << "Client is listeing at port " << port_num << " - REQ #6" << endl;
			this->clientMsgHandler(msgQ);
		}
		catch (...)
		{
			cout << "Error in starting client" << endl;
		}
	}
	);
	t2.detach();
	thread t([&]() {
		cout << "Client is listening for GUI messages" << endl;
			while (true)
			{
				string guiMsg = this->getMessageRQ();				
				unordered_map<string, string> map = parseGuiMsg(guiMsg);
				processGuiMsg(map);
			}
		}
	);
	t.detach();
	
}

#ifdef TEST_CLIENT
int main()
{
	::SetConsoleTitle(L"Client");
	Client c;
	SocketSystem ss;
	SocketConnecter si;
	SocketListener sl(8081, Socket::IP6);
	Async::BlockingQueue<HttpMessage> msgQ;
	ClientReceiver ch(msgQ);
	sl.start(ch);
	cout << "In client main" << endl;
	std::string argstr = " NULL, ..\\files, *.h, *.cpp";
	while (!si.connect("localhost", 8080))
	{
		std::cout << "\n client waiting to connect";
		::Sleep(100);
	}
	HttpMessage msg;
	string filespath = FileSystem::Path::getFullFileSpec("../files/");
	vector<string> files = FileSystem::Directory::getFiles("../files");
	cout << "Files list\n";
	for (auto file : files)
		cout << filespath + file << endl;
	for (auto file : files)
	{
		if (c.sendFile(filespath + file, "files", "localhost:8080", si))
			cout << "File sent : " + file << endl;
	}
	msg = c.makeMessage(1, argstr, "localhost:8080");
	c.sendMessage(msg, si);
	msg = c.makeMessage(3, "*.htm", "localhost:8080");
	c.sendMessage(msg, si);
	msg.clear();
	msg = c.makeMessage("DirsReq", "", "localhost:8080");
	c.sendMessage(msg, si);
	thread msgHandlerThread(clientMsgHandler, std::ref(msgQ));
	msgHandlerThread.join();
	GuiCommunicator gui;
	clientMsgHandler(msgQ);

	GuiComm gc;
	while (true)
	{
		string msg = gc.getMessageRQ();
		cout << "Msg received from GUI : \n" + msg << endl;
		//::Sleep(1000);
		gc.postMessageSQ("Client Bounced back : " + msg);
	}
}
#endif // TEST_CLIENT
