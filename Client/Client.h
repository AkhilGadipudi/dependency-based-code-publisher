#pragma once
/////////////////////////////////////////////////////////////////////
// Client.h - Used to communicate with remote publisher				//
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu          //
// CSE687 - OOD, Spring 2017									   //
/////////////////////////////////////////////////////////////////////
/*
Operation :
Provides three classes
Client - To communicate with the remote code publisher
ClientReceiver - Handles messages recived from code publisher, given to socket listner
GuiComm - Handles communication with the ClientGUI

Public Interface :
Client c(8080);		//Takes client port number as argument
c.makeMessage(postType, strMsg, server_endpoint)		//Create httpmessage,
												postType should be selected based on the use
												and we make sure server know what to do for a given postType
c.sendFile(fqname, category, srv_endpoint, socket)		// sends file to store in folder spcified to publisher
c.sendMessage(httpMessage, socket)		// sends httpsmessage thought an already connected socket


ClientReceiver cr(blockingQ) // class with operator() method, directly used by socketlistner
				member funtions readMessage and readFile are called by operator() not used by user directly

GuiComm // Only used though gui, so no object creadded directly, used by gui through IClient object
		which is made available to gui though CLIShim
		find usage in IClient public interface

Required Files:
HttpMessage.h, HttpMessage.cpp
Sockets.h, Sockets.cpp
IClient.h
FileSyste.h, FileSystem.cpp
Utilities.h, Utilities.cpp

Build Command:
devenv Client.sln /rebuild debug

Maintanance History:
Ver 1.0 - May 4,2017 - Initial Release
*/
#include"../HttpMessage/HttpMessage.h"
#include"../Sockets/Sockets.h"
#include"IClient.h"
#include<unordered_map>
using namespace std;
// Communicates with remote code publisher
class Client
{
public:
	Client() {}
	Client(string port)
	{
		myEndPoint = "localhost:" + port;
	}
	using EndPoint = std::string;
	HttpMessage makeMessage(std::string postType, const std::string& msgBody, const EndPoint& ep);
	void sendMessage(HttpMessage& msg, Socket& socket);
	bool sendFile(const std::string& fqname,const std::string& category, const EndPoint& ep, Socket& socket);
private:
	EndPoint myEndPoint = "";
};

// Handles messages received from remote code publisher
class ClientReceiver
{
public:
	ClientReceiver(Async::BlockingQueue<HttpMessage>& msgQ) : msgQ_(msgQ) {}
	void operator()(Socket socket);
private:
	bool connectionClosed_;
	HttpMessage readMessage(Socket& socket);
	bool readFile(HttpMessage& msg, Socket& socket);
	Async::BlockingQueue<HttpMessage>& msgQ_;
};


// Handles communication with ClientGUI
class GuiComm : public IClient
{
public:
	GuiComm() { }
	virtual std::string getMessageSQ() override;
	virtual void postMessageRQ(std::string msg) override;
	virtual void start(std::string port) override;
	virtual void postMessageSQ(std::string msg) override;
	virtual std::string getMessageRQ() override;
	void processGuiMsg(unordered_map<string, string> map);
	unordered_map<string,string> parseGuiMsg(string msg);
	Client client;
	void clientMsgHandler(Async::BlockingQueue<HttpMessage>& msgQ);
private:
	Async::BlockingQueue<string> sendQ;
	Async::BlockingQueue<string> recvQ;
	void helper_uploadfiles(unordered_map<string, string> map);
	void helper_publish(unordered_map<string, string> map);
	void helper_dloadfiles(unordered_map<string, string> map);
	void helper_pubfileslist();
};
