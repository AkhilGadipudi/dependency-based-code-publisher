#pragma once
//////////////////////////////////////////////////////////////////////////////
// IClient -  Interface for CLIshim to talk to GuiComm written in natic c++	 //
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu					//
// CSE687 - OOD, Spring 2017												 //
///////////////////////////////////////////////////////////////////////////////
/*
Operation: Provide interface for CLIShim to use GuiComm class writter in natice c++
Public Interface:

ObjectFactory f;
IClient* c = f.createClient();
c->start(portnumber);		//provide pornt number for client, here obtained from commandline args
c->postMessageRQ(str);		//send message to client receiveQ
string s = c->getMessageSQ();	//get message from client's send queue

Maintanance History:
Ver 1.0 - May 4,2017 - Initial Release
*/
#ifdef IN_DLL
#define DLL_DECL __declspec(dllexport)
#else
#define DLL_DECL __declspec(dllimport)
#endif

#include <string>

class IClient {
public:	
	virtual std::string getMessageSQ() = 0;
	virtual void postMessageRQ(std::string msg) = 0;
	virtual void start(std::string port) = 0;
	virtual void postMessageSQ(std::string msg) = 0;
	virtual std::string getMessageRQ() = 0;
};
extern "C" {
	struct ObjectFactory {
		DLL_DECL IClient* createClient();
	};
}